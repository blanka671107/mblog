"""mblog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from mainsite.views import homepage, showpost, author,post_times
from mainsite.views import world, about, course, carlist, carprice
#from signup.views import about,listing,disp_detail
from signup.views import listing,disp_detail
import signup.urls

#from sitesite.views import site


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', homepage),
    path('Hello/', world),
#    path('ssii', site),
    path('post/<slug:slug>', showpost),
#    path('about/',about),
    #re_path(r'^about',about),
    #path('list/',listing),
    #re_path(r'^list/$',listing),
    #path('list/<str:id>',disp_detail),
    #re_path(r'^list/([0|1|2|3|4])/$',disp_detail),
    #re_path(r'^list/(\d+)/$',disp_detail),
    #path('author/',author),
    #path('author/',about),
    path('author/',about, {'author_id':'paza19870000'}),
    path('author/<str:author_id>',about),
    path('post-time/<int:yr>/<int:mon>/<int:day>/<int:post_num>',post_times, name='post-yrl'),
    #re_path(r'^post-time/(?P<yr>[1|2][9|0]\d{2})/(?P<mon>[0|1]?\d)/(?P<day>[0|1|2]\d)/(?P<post_num>\d+)$',post_times),
    path('product/', include(signup.urls)),
    path('course/', course),
    path('course/<int:course_no>', course, name='course-url'),  
    path('carlist/', carlist),
    path('carlist/<int:maker>/', carlist, name='carlist-url'),
    path('carprice/', carprice),
    path('carprice/<int:maker>/', carprice, name='carlist-url'),
]
