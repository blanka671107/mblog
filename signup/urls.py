from django.urls import path, re_path
from signup.views import listing,disp_detail

urlpatterns=[
    re_path(r'^list/$',listing),
    re_path(r'^list/(\d+)/$',disp_detail),
    
]