from django.db import models
#from django.utils import timezone

# Create your models here.

class data_demo(models.Model):
    big_int_f = models.BigIntegerField()
    bool_f = models.BooleanField()
    date_f = models.DateField(auto_now=True)
    char_f = models.CharField(max_length=200)
    datatime_f = models.DateTimeField(auto_now_add=True)
    decimal_f = models.DecimalField(max_digits=10, decimal_places=2)
    float_f = models.FloatField(null=True)
    int_f = models.IntegerField(default=2020)
    text_f = models.TextField()


class userinfo(models.Model):
    dept_select={
        ('IT','資訊類'),
        ('HR','人資課'),
        ('RD','研發克'),
        ('PR','公關課')
    }


    uid = models.CharField(max_length=20,unique=True,verbose_name="account")
    pwd = models.CharField(max_length=128)
    email =models.EmailField(max_length=254)
    signup_time = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    id_no = models.CharField(max_length=10)
    cellphone = models.CharField(max_length=10)
    address = models.TextField()
    webpage = models.URLField()
    dept = models.TextField(max_length=2,default='IT',choices=dept_select)
    st = models.BooleanField()

    def __str__(self):
        return self.uid    

class product(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField()
    qty  = models.IntegerField()

    def __str__(self):
        return self.name