# Generated by Django 3.0.3 on 2020-05-13 06:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('signup', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userinfo',
            name='dept',
            field=models.TextField(choices=[('HR', '人資課'), ('IT', '資訊類'), ('PR', '公關課'), ('RD', '研發克')], default='IT', max_length=2),
        ),
    ]
