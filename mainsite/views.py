from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Post
from datetime import datetime
from django.urls import reverse

# Create your views here.
def homepage(request):
    posts = Post.objects.all()
 #   post_lists = list()
 #   for count, post in enumerate(posts):
 #       post_lists.append("No.{}:".format(str(count)) + str(post)+"<br>") 
 #   return HttpResponse(post_lists)
    now = datetime.now()
    return render(request, 'index.html', locals())


def world(request):
   # osts = Post.objects.all()
    post_lists = list()
    post_lists.append("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj")
    #for count, post in enumerate(posts):
     #   post_lists.append("No.{}:".format(str(count)) + str(post)+"<br>") 
    return HttpResponse(post_lists)


def showpost(request, slug):
    try:
        post = Post.objects.get(slug = slug)
        if post != None :
            return render(request, 'post.html', locals())
    except:
        return redirect('/')

def author(requst):
    #dom="<h2>Here is author page</h2><hr>".format(author_id)
    dom="<h2>Here is author page</h2><hr>"
    return HttpResponse(dom)

def post_times(request, yr, mon, day, post_num):
    dom="<h2> {}/{}/{}:Post #{}</h2><hr>".format(yr,mon,day,post_num)
    dom=dom+"<br><r><br>"
    dom=dom+"{}".format(reverse('post-url',args=("2020","12","1","01")))
    return HttpResponse(dom)

#def about(request,author_id="zozo19841011"):
def about(request,author_id="zozo19841011"):
    dom="<h2> Here is Author: {}'s about page</h2><hr>".format(author_id)
    return HttpResponse(dom)

def course(request, course_no=0):
    course_list=[
        {"name":"A1","tvcode":"vI4KEk828Sc"},
        {"name":"A2","tvcode":"8JDWk1i88Vs"},
        {"name":"A3","tvcode":"wS6cw3x2mtE"},
        {"name":"A4","tvcode":"Rhz1AqkWIDg"},
        {"name":"A5","tvcode":"WIYvTXo6fFE"},
        {"name":"A6","tvcode":"cAKnTSJb-SE"}
    ]
    now = datetime.now()
    course_no = course_no
    course = course_list[course_no]
    hour= now.timetuple().tm_hour
    #return render(request, 'live.html', locals())
    return render(request, 'live_re.html', locals())

def carlist(request, maker=0):
    car_maker = ['SAAB', 'Ford', 'Honda', 'Mazda', 'Nissan','Toyota' ]
    car_list =[ 
        [],
        ['Fiesta', 'Focus', 'Modeo', 'EcoSport', 'Kuga', 'Mustang'],
        ['Fit', 'Odyssey', 'CR-V', 'City', 'NSX'],
        ['Mazda3', 'Mazda5', 'Mazda6', 'CX-3', 'CX-5', 'MX-5'],
        ['Tida', 'March', 'Livina', 'Sentra', 'Teana', 'X-Trail', 'Juke', 'Murano'],
        ['Camry','Altis','Yaris','86','Prius','Vios', 'RAV4', 'Wish']
    ]
    maker = maker
    maker_name = car_maker[maker]
    cars=car_list[maker]
    return render(request, 'carlist.html',locals())


def carprice(request, maker=0):
    car_maker = ['Ford', 'Honda', 'Mazda']
    car_list = [
        [
            {'model':'Fiesta', 'price': 203500}, 
            {'model':'Focus', 'price': 605000}, 
            {'model':'Mustang', 'price': 900000} 
        ],
        [ 
            {'model':'Fit', 'price': 450000},
            {'model':'City', 'price': 150000},
            {'model':'NSX', 'price':1200000}
        ],
        [
            {'model':'Mazda3', 'price': 329999},
            {'model':'Mazda5', 'price': 603000},
            {'model':'Mazda6', 'price':850000}
        ],
    ]
    maker = maker
    maker_name = car_maker[maker]
    cars = car_list[maker]
    return render(request, 'carprice.html', locals())
