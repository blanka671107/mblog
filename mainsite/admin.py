from django.contrib import admin
from .models import Post
from signup.models import data_demo
from signup.models import userinfo
from signup.models import product
# Register your models here.

class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'pub_date')

admin.site.register(Post, PostAdmin)
admin.site.register(data_demo)
admin.site.register(userinfo)
admin.site.register(product)


#admin.site.register(Post)

# Register your models here.
